package com.gwylim.system.events.spring.annotations.processors;

import com.gwylim.common.ReflectionUtil;
import com.gwylim.system.events.api.EventBroadcaster;
import com.gwylim.system.events.spring.annotations.GlobalEventOnDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EventOnDestroyAnnotationProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(EventOnDestroyAnnotationProcessor.class);

    private final EventBroadcaster eventBroadcaster;

    @Autowired
    public EventOnDestroyAnnotationProcessor(final EventBroadcaster eventBroadcaster) {
        this.eventBroadcaster = eventBroadcaster;
    }

    public void register(final Object bean) {
        Arrays.stream(bean.getClass().getMethods())
              .filter(EventOnDestroyAnnotationProcessor::hasAnnotation)
              .forEach(this::preCheckMethod);
    }


    public void unregister(final Object bean) {
        Arrays.stream(bean.getClass().getMethods())
                .filter(EventOnDestroyAnnotationProcessor::hasAnnotation)
                .forEach(m -> this.sendBroadcastEvent(bean, m));
    }



    /* PUBLIC INTERFACE ABOVE */

    private void sendBroadcastEvent(final Object bean, final Method m) {
        preCheckMethod(m);

        ReflectionUtil.invokeToSet(bean, m)
                      .forEach(eventBroadcaster::globalBroadcast);
    }

    private void preCheckMethod(final Method method) {
        final String methodIdentifier = ReflectionUtil.getMethodIdentifier(method);

        if (method.getParameterCount() != 0) {
            throw new IllegalStateException("@GlobalEventOnDestroy methods must have no params. " + methodIdentifier + " has " + method.getParameterCount());

        }
        LOG.info("Observed @GlobalEventOnDestroy method " + methodIdentifier);
    }

    private static boolean hasAnnotation(final Method method) {
        return ReflectionUtil.hasAnnotation(method, GlobalEventOnDestroy.class);
    }
}

