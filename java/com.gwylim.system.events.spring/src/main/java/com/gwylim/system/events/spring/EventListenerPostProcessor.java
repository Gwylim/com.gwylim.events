package com.gwylim.system.events.spring;

import com.gwylim.system.events.spring.annotations.processors.EventListenerAnnotationProcessor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EventListenerPostProcessor implements DestructionAwareBeanPostProcessor {

    private EventListenerAnnotationProcessor eventListenerAnnotationProcessor;

    @Autowired
    public EventListenerPostProcessor(final EventListenerAnnotationProcessor eventListenerAnnotationProcessor) {
        this.eventListenerAnnotationProcessor = eventListenerAnnotationProcessor;
    }

    @Override
    public Object postProcessAfterInitialization(@Nonnull final Object bean, final String beanName) throws BeansException {
        eventListenerAnnotationProcessor.register(bean);
        return bean;
    }


    @Override
    public void postProcessBeforeDestruction(@Nonnull final Object bean, @Nonnull final String beanName) throws BeansException {
        eventListenerAnnotationProcessor.unregister(bean);
    }
}

