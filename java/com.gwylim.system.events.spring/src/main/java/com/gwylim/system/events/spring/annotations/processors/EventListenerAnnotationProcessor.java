package com.gwylim.system.events.spring.annotations.processors;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.gwylim.common.ReflectionUtil;
import com.gwylim.system.events.api.EventSystem;
import com.gwylim.system.events.api.Listener;
import com.gwylim.system.events.api.annotations.EventListener;
import com.gwylim.system.events.api.annotations.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EventListenerAnnotationProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(EventListenerAnnotationProcessor.class);

    // TODO: This can probably be weak so we drop listeners for objects that have been garbage collected.
    private final ListMultimap<Object, Listener<?>> listeners = ArrayListMultimap.create();

    private final EventSystem globalEventSystem;

    @Autowired
    public EventListenerAnnotationProcessor(final EventSystem globalEventSystem) {
        this.globalEventSystem = globalEventSystem;
    }

    public void register(@Nonnull final Object bean) {
        final Set<Method> methods = Arrays.stream(bean.getClass().getMethods())
                                          .filter(EventListenerAnnotationProcessor::hasAnnotation)
                                          .collect(Collectors.toSet());

        if (!methods.isEmpty()) {
            final Set<Object> subjects = getSubjects(bean);
            methods.forEach(m -> this.registerListener(bean, m, subjects));
        }
    }




    public void unregister(@Nonnull final Object bean) {
        unregisterListeners(bean);
    }


    /* PUBLIC API ABOVE */


    /**
     * Find the subjects for the bean being registered using @Subject annotated methods.
     */
    private Set<Object> getSubjects(final Object bean) {
        final Set<Method> methods = Arrays.stream(bean.getClass().getMethods())
                                          .filter(EventListenerAnnotationProcessor::hasSubjectAnnotation)
                                          .collect(Collectors.toSet());
        return methods.stream()
               .flatMap(m -> ReflectionUtil.invokeToSet(bean, m).stream())
               .collect(Collectors.toSet());
    }

    private void registerListener(final Object bean, final Method method, final Set<Object> subjects) {
        final Class<?>[] parameterTypes = method.getParameterTypes();
        final Class<?> declaringClass = bean.getClass();

        if (parameterTypes.length != 1) {
            throw new IllegalStateException("@Listener methods must have exactly one parameter. " + declaringClass.getSimpleName() + "#" + method.getName() + " has " + parameterTypes.length);
        }

        final Class<?> parameterType = parameterTypes[0];

        final Listener<?> listener = createListener(method, bean, parameterType, subjects);
        registerListener(bean, listener);

        LOG.info("Registered " + declaringClass.getSimpleName() + "#" + bean.hashCode() + "." + method.getName() + " as Listener for " + parameterType.getSimpleName());
    }

    private <T> Listener<T> createListener(final Method method, final Object bean, final Class<T> type, final Set<Object> subjects) {
        return Listener.of(type, event -> invoke(method, bean, event), subjects);
    }

    private <T> void invoke(final Method method, final Object bean, final T event) {
        try {
            method.invoke(bean, event);
        }
        catch (final Exception e) {
            LOG.error("Failed to invoke " + ReflectionUtil.getMethodIdentifier(method) + " with " + event.toString(), e);
        }
    }


    /**
     * Register a listener against the bean and keep a reference to it so it is not garbage collected until the bean is.
     */
    private void registerListener(final Object bean, final Listener<?> listener) {
        synchronized (listeners) {
            globalEventSystem.register(listener);
            listeners.put(bean, listener);
        }
    }

    /**
     * Unregister all the listeners associated with a bean.
     */
    private void unregisterListeners(final Object bean) {
        synchronized (listeners) {
            final List<Listener<?>> generatedListeners = listeners.removeAll(bean);
            if (!generatedListeners.isEmpty()) {
                LOG.info("Unregister " + generatedListeners.size() + " listeners for: " + bean.getClass().getSimpleName());
                generatedListeners.forEach(globalEventSystem::unregister);
            }
        }
    }

    private static boolean hasAnnotation(final Method method) {
        return ReflectionUtil.hasAnnotation(method, EventListener.class);
    }

    private static boolean hasSubjectAnnotation(final Method method) {
        return ReflectionUtil.hasAnnotation(method, Subject.class);
    }
}

