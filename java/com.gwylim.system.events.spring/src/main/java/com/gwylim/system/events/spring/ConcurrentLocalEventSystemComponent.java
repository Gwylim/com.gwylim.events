package com.gwylim.system.events.spring;

import com.gwylim.system.events.impl.ConcurrentLocalEventSystem;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Spring component wrapper for the EventSystem Implementation.
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ConcurrentLocalEventSystemComponent extends ConcurrentLocalEventSystem implements DisposableBean {

    @PostConstruct
    public void init() {
        // Once constructed start the message system.
        start();
    }

    @Override
    public void destroy() {
        terminate();
    }
}
