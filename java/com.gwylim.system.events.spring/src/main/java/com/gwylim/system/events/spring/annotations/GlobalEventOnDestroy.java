package com.gwylim.system.events.spring.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Auto-registration of a method as the source of an event when the object is destroyed.
 * The object containing the annotation MUST be managed by spring.
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface GlobalEventOnDestroy {

}

