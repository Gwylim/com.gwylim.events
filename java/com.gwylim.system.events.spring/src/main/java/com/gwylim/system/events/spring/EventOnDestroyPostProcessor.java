package com.gwylim.system.events.spring;

import com.gwylim.system.events.spring.annotations.processors.EventOnDestroyAnnotationProcessor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EventOnDestroyPostProcessor implements DestructionAwareBeanPostProcessor {
    private EventOnDestroyAnnotationProcessor eventOnDestroyAnnotationProcessor;

    @Autowired
    public EventOnDestroyPostProcessor(final EventOnDestroyAnnotationProcessor eventOnDestroyAnnotationProcessor) {
        this.eventOnDestroyAnnotationProcessor = eventOnDestroyAnnotationProcessor;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) throws BeansException {
        eventOnDestroyAnnotationProcessor.register(bean);
        return bean;
    }

    @Override
    public void postProcessBeforeDestruction(final Object bean, final String beanName) throws BeansException {
        eventOnDestroyAnnotationProcessor.unregister(bean);
    }
}

