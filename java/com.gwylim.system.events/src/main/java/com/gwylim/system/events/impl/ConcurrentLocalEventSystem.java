package com.gwylim.system.events.impl;

import com.gwylim.system.events.api.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Global event broadcaster;
 * Register Listeners and publish global events.
 *
 * Listeners will receive only events of the type they register for.
 *
 * Listeners who do not have any subjects will receive all events.
 * Listeners with a subjects will receive only events with at least one matching subject.
 */
public class ConcurrentLocalEventSystem extends AbstractEventSystem {
    private static final Logger LOG = LoggerFactory.getLogger(ConcurrentLocalEventSystem.class);

    /**
     * Warn if listener execution takes longer than this number of milliseconds.
     */
    private static final int LISTENER_EXECUTION_TIME_WARNING_LIMIT_MS = 10;

    private static final String LISTENER_THREAD_NAME_PREFIX = "Post-Run-";
    private static final String EVENT_POSTMAN_THREAD_NAME = "Event-Postman";
    private static final int THREAD_NAME_MAX_LENGTH = 15;


    private final BlockingQueue<Object> queue = new LinkedBlockingQueue<>();
    private final Thread thread;

    private final AtomicBoolean terminate = new AtomicBoolean(false);

    public ConcurrentLocalEventSystem() {
        thread = new Thread(() -> {
            try {
                while (!terminate.get()) {
                    doBroadcast(queue.take());
                }
            }
            catch (final InterruptedException e) {
                LOG.info("Postman thread interrupted. Terminating.");
            }
        }, EVENT_POSTMAN_THREAD_NAME);

        thread.setDaemon(true);
    }

    @Override
    public void start() {
        thread.start();
    }

    protected void terminate() {
        terminate.set(true);
        thread.interrupt();
        try {
            thread.join();
        }
        catch (final InterruptedException e) {
            LOG.info("Interrupted waiting for postman to terminate.");
        }
    }

    /**
     * Broadcast an event to all listeners for that type and subject.
     */
    @Override
    public synchronized void globalBroadcast(final Object event) {
        try {
            queue.put(event);
        }
        catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Execute a listener in its own thread to make sure synchronised blocks are respected
     * and the postman does not exit this class.
     * @param event The event the listener will receive.
     * @param l     The listener to execute.
     */
    @SuppressWarnings("unchecked")
    protected void runListener(final Object event, final Listener l) {
        final LocalDateTime start = LocalDateTime.now();
        final Thread thread = new Thread(() -> l.receive(event), generateListenerThreadName(l));
        thread.start();
        try {
            thread.join();
        }
        catch (final InterruptedException e) {
            LOG.info("Postman thread interrupted waiting for listener to complete.");
            Thread.currentThread().interrupt();
        }
        final long ms = start.until(LocalDateTime.now(), ChronoUnit.MILLIS);
        if (ms > LISTENER_EXECUTION_TIME_WARNING_LIMIT_MS) {
            LOG.warn("Listener execution took " + ms + "ms");
        }
        else {
            LOG.debug("Listener execution took " + ms + "ms");
        }
    }

    private String generateListenerThreadName(final Listener l) {
        return (LISTENER_THREAD_NAME_PREFIX + l.hashCode()).substring(0, THREAD_NAME_MAX_LENGTH);
    }
}
