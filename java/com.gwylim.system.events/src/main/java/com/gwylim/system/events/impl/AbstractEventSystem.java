package com.gwylim.system.events.impl;

import com.google.common.collect.Sets;
import com.gwylim.common.ReflectionUtil;
import com.gwylim.system.events.api.EventSystem;
import com.gwylim.system.events.api.Listener;
import com.gwylim.system.events.api.annotations.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * Global event broadcaster;
 * Register Listeners and publish global events.
 *
 * Listeners will receive only events of the type they register for.
 *
 * Listeners who do not have any subjects will receive all events.
 * Listeners with a subjects will receive only events with at least one matching subject.
 */
abstract class AbstractEventSystem implements EventSystem {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractEventSystem.class);

    /**
     * Listeners who do not have any subjects will listen to all global events.
     * So all global events are sent to NO_SUBJECT listeners + listeners for their subject.
     * Listeners are added to NO_SUBJECT only if they have NO subjects.
     */
    private static final Object NO_SUBJECT = new Object();

    // Event type -> Subject -> Listener
    private final ConcurrentHashMap<Class<?>, Map<Object, Set<Listener<?>>>> listenersForClasses = new ConcurrentHashMap<>();

    /**
     * Override to perform startup tasks.
     */
    public void start() {
        // No-op by default.
    }

    /**
     * Override this method to perform cleanup.
     */
    protected abstract void terminate();

    @SuppressWarnings("deprecation")
    @Override
    protected void finalize() {
        terminate();
    }

    /** Register a listener. */
    @Override
    public synchronized <T> Listener<T> register(final Listener<T> listener) {
        if (listener == null) {
            return null;
        }

        final Class<?> type = getType(listener);
        LOG.info("Registering listener: " + listener.getClass().getSimpleName() + " listening for " + type.getSimpleName() + " with subjects: " + listener.subjects());

        // Weak because if the subject goes out of scope there will be no way to call the listener anyway:
        final Map<Object, Set<Listener<?>>> listeners = listenersForClasses.computeIfAbsent(type, k -> new WeakHashMap<>());
        if (listener.subjects().isEmpty()) {
            final Set<Listener<?>> subjectListeners = listeners.computeIfAbsent(NO_SUBJECT, k -> new LinkedHashSet<>());
            subjectListeners.add(listener);
        }
        else {
            listener.subjects().forEach(s -> {
                final Set<Listener<?>> subjectListeners = listeners.computeIfAbsent(s, k -> new LinkedHashSet<>());
                subjectListeners.add(listener);
            });
        }

        return listener;
    }


    /** Unregister a listener. */
    @Override
    public synchronized <T> Listener<T> unregister(final Listener<T> listener) {
        if (listener == null) {
            return null;
        }

        final Class<?> type = getType(listener);
        LOG.info("Unregistering listener: " + listener.getClass().getSimpleName() + " for type " + type.getSimpleName());
        Optional.ofNullable(listenersForClasses.get(type)).ifPresent(listeners -> {
            if (listener.subjects().isEmpty()) {
                Optional.ofNullable(listeners.get(NO_SUBJECT))
                                             .ifPresent(s -> s.remove(listener));
            }
            else {
                listener.subjects().forEach(subjectMap ->
                    Optional.ofNullable(listeners.get(subjectMap))
                                                 .ifPresent(s -> s.remove(listener)));
            }
        });

        return listener;
    }

    /**
     * Override to implement the broadcast of an event.
     * The implementation must through some means call doBroadcast(event).
     */
    @Override
    public abstract void globalBroadcast(Object event);




    /* PUBLIC INTERFACE ABOVE THIS LINE */

    /**
     * Override to implement the execution of a listener against an event.
     * Must eventually call listener.receive(event)
     */
    protected abstract void runListener(Object event, Listener<?> listener);

    /* EXTENSION POINTS ABOVE THIS LINE */


    protected final void doBroadcast(final Object event) {
        final Class<?> type = event.getClass();
        final LinkedHashSet<Class<?>> allClasses = ReflectionUtil.getSuperTypes(type);

        allClasses.forEach(t -> doBroadcastForType(event, t));
    }


    private void doBroadcastForType(final Object event, final Class<?> type) {
        final Set<Object> eventSubjects = getSubjects(event);
        final Set<Object> subjects = Sets.union(eventSubjects, Collections.singleton(NO_SUBJECT));

        final Map<Object, Set<Listener<?>>> subjectToListeners = listenersForClasses.get(type);
        if (subjectToListeners != null) {
            final Set<Listener<?>> listeners = subjects.stream()
                                                       .flatMap(s -> subjectToListeners.getOrDefault(s, Collections.emptySet()).stream())
                                                       .collect(Collectors.toSet());

            LOG.info("Broadcasting Event " + type.getSimpleName() + " to " + listeners.size() + " listeners based on subjects: " + eventSubjects);
            listeners.forEach(l -> runListener(event, l));
        }
        else {
            LOG.warn("No listeners for event type " + type.getSimpleName() + ".");
        }
    }


    /** Get the subjects for an event. */
    private static Set<Object> getSubjects(final Object event) {
        return Arrays.stream(event.getClass().getMethods())
                     .filter(m -> ReflectionUtil.hasAnnotation(m, Subject.class))
                     .flatMap(m -> ReflectionUtil.invokeToSet(event, m).stream())
                     .collect(Collectors.toSet());
    }


    private static Class<?> getType(final Listener<?> listener) {
        final Class<?> type = listener.type();
        if (type == null) {
            throw new IllegalArgumentException("Listener " + listener.getClass().getSimpleName() + " does not specify the type it is listening for.");
        }
        return type;
    }
}
