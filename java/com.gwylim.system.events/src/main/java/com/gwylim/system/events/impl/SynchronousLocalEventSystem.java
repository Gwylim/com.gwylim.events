package com.gwylim.system.events.impl;

import com.gwylim.system.events.api.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Global event broadcaster;
 * Register Listeners and publish global events.
 *
 * Listeners will receive only events of the type they register for.
 *
 * Listeners who do not have any subjects will receive all events.
 * Listeners with a subjects will receive only events with at least one matching subject.
 */
public class SynchronousLocalEventSystem extends AbstractEventSystem {
    private static final Logger LOG = LoggerFactory.getLogger(SynchronousLocalEventSystem.class);


    public SynchronousLocalEventSystem() {
    }

    protected void terminate() {
        // No cleanup required.
    }

    /**
     * Broadcast an event to all listeners for that type and subject.
     */
    @Override
    public synchronized void globalBroadcast(final Object event) {
        doBroadcast(event);
    }

    /**
     * Execute a listener in its own thread to make sure synchronised blocks are respected
     * and the postman does not exit this class.
     * @param event The event the listener will receive.
     * @param l     The listener to execute.
     */
    @SuppressWarnings("unchecked")
    protected void runListener(final Object event, final Listener l) {
        l.receive(event);
    }
}
