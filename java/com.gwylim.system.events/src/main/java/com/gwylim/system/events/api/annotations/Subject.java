package com.gwylim.system.events.api.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to mark fields of events, returned objects form those methods will be used as the subject of the event.
 * The event will only trigger listeners that either have one of the subjects or no required subjects.
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Subject {
}
