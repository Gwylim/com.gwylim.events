package com.gwylim.system.events.api;

public interface EventSystem extends EventBroadcaster {

    /** Register a listener. */
    <T> Listener<T> register(Listener<T> listener);

    /** Un-register a listener. */
    <T> Listener<T> unregister(Listener<T> listener);

}
