package com.gwylim.system.events.api;

import com.gwylim.common.ReflectionUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@FunctionalInterface
public interface Listener<T> {

    /** Called when the event in question is fired. */
    void receive(T event);

    /** Override to indicate which subjects this Listener is interested in.
     *  Annotate the event type T methods to indicate what are subjects of the event.
     *  The listener will only be called for events with at least one subject matching one provided by this method.
     */
    default Set<Object> subjects() {
        return Collections.emptySet();
    }

    /**
     * The type that this Listener receives.
     * For compile time Listener types where T is bound this method does not need to be overridden.
     */
    @SuppressWarnings("unchecked")
    default Class<T> type() {
        return (Class<T>) ReflectionUtil.getGenericClass(this.getClass(), Listener.class);
    }

    /** Create a listener for a class with the given subjects. */
    static <T> Listener<T> of(final Class<T> type, final Consumer<T> listener, final Object... subjects) {
        final Set<Object> subjectSet = Arrays.stream(subjects).collect(Collectors.toSet());
        return of(type, listener, subjectSet);
    }

    static <T> Listener<T> of(final Class<T> type, final Consumer<T> listener, final Set<Object> subjectSet) {
        return new Listener<T>() {
            @Override
            public void receive(final T event) {
                listener.accept(event);
            }

            @Override
            public Set<Object> subjects() {
                return subjectSet;
            }

            @Override
            public Class<T> type() {
                return type;
            }
        };
    }
}
