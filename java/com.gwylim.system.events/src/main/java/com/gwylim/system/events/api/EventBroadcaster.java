package com.gwylim.system.events.api;

import com.gwylim.common.ThreadUtil;

import java.time.Duration;

public interface EventBroadcaster {

    /** Fire a global event that will be received by all appropriate listeners. */
    @SuppressWarnings("Unchecked")
    void globalBroadcast(Object event);

    /** Fire a global event that will be received by all appropriate listeners. */
    @SuppressWarnings("Unchecked")
    default void globalBroadcast(final Object event, final Duration delay) {
        ThreadUtil.run(delay, () -> globalBroadcast(event));
    }
}
